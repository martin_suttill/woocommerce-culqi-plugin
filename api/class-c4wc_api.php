<?php
/**
 * Functions for interfacing with Culqi's API
 * 
 * @class       C4WC_API
 * @author      Martin Suttill
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

class C4WC_API {

	protected $api_endpoint = 'https://integ-pago.culqi.com';
	protected $commerce_code = 'demo';
	protected $secret_key = 'JlhLlpOB5s1aS6upiioJkmdQ0OYZ6HLS2+/o4iYO2MQ=';

	public function __construct($api_endpoint, $commerce_code, $secret_key)
	{
		$this->api_endpoint = $api_endpoint;
		$this->commerce_code = $commerce_code;
		$this->secret_key = $secret_key;

		Culqi::$servidorBase = $this->api_endpoint;
		Culqi::$codigoComercio = $this->commerce_code;
		Culqi::$llaveSecreta = $this->secret_key;
	}

	public function create_data_for_payment($order = array(), $amount, $currency)
	{
		$orderInfo = "";

		try {
			$data = Pago::crearDatospago(array(
				// Order number
				Pago::PARAM_NUM_PEDIDO => $order['order_id'],
				// Order currency ("PEN" or "USD")
				Pago::PARAM_MONEDA => $currency,
				// Order Amount (Without floating points)
				Pago::PARAM_MONTO => $amount,
				// Order description
				Pago::PARAM_DESCRIPCION => $order['order_description'],
				// Customer's Country Code
				Pago::PARAM_COD_PAIS => $order['billing_country'],
				// Customer's City
				Pago::PARAM_CIUDAD => $order['billing_city'],
				// Customer's address
				Pago::PARAM_DIRECCION => $order['billing_address_1'] . " " . $order['billing_address_2'],
				// Customre's phone number
				Pago::PARAM_NUM_TEL => $order['billing_phone'],
				// Customer's email
				"correo_electronico" => $order['billing_email'],
				// Customer's id
				"id_usuario_comercio" => $order['customer_id'],
				// Customer's name
				"nombres" => $order['billing_first_name'],
				// Customer's lastname
				"apellidos" => $order['billing_last_name']
			));
			// Cyphered string, Culqi's order creation response
			$orderInfo = $data[Pago::PARAM_INFO_VENTA];
			error_log('Código Respuesta : ' . $data['codigo_respuesta'] . "\n" . 'Mensaje Respuesta : ' . $data['mensaje_respuesta'] . "\n", 3,  plugin_dir_path( __FILE__ ) . '../info.log');
		} catch (InvalidParamsException $e) {
			$orderInfo = $e->getMessage();
		}

		return $orderInfo;
	}

	public function decipher_response($response)
	{
		return Culqi::decifrar($response);
	}

	public function refund($transaction_id)
	{
		$data = "";
		try {
			$data = Pago::anular($transaction_id);
		} catch (Exception $e) {
			$data = $e->getMessage();
		}
		return $data;
	}
}