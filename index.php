<?php
/**
 * Plugin Name: Culqi Payment Gateway
 * Plugin URI: http://www.54solutions.com/
 * Description: Culqi Payment Gateway for WooCommerce
 * Version: 1.0
 * Author: FIFTY4 Solutions
 * Author URI: http://www.54solutions.com/
 * Text Domain: culqi
 * Domain Path: /languages
 */

require('vendor/culqi.php');
require('api/class-c4wc_api.php');

add_action('plugins_loaded', 'culqi_init', 0);

function culqi_init() {
    if (!class_exists('WC_Payment_Gateway')) return;
    load_plugin_textdomain( 'culqi', false, basename( dirname( __FILE__ ) ) . '/languages/' );

    class WC_Culqi extends WC_Payment_Gateway {

        public function __construct()
        {
            $this->id = 'culqi';
            $this->method_title = 'Culqi';
            $this->has_fields = false;
            $this->supports = array(
                'refunds'
            );

            $this->init_form_fields();
            $this->init_settings();

            $this->enabled = $this->settings['enabled'];
            $this->title = $this->settings['title'];
            $this->description = $this->settings['description'];
            $this->icon 		      = WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/assets/images/credits.png';
            $this->testing = $this->settings['testing'];
            $this->commerce_code = ($this->testing == 'yes') ? $this->settings['test_commerce_code'] : $this->settings['live_commerce_code'];
            $this->secret_key = ($this->testing == 'yes') ? $this->settings['test_secret_key'] : $this->settings['live_secret_key'];
            $this->endpoint = ($this->testing == 'yes') ?  $this->settings['test_api_endpoint'] : $this->settings['live_api_endpoint'];
            $this->redirect_page_id = $this->settings['redirect_page_id'];
            $this->timeout_message_title = $this->settings['timeout_message_title'];
            $this->timeout_message = $this->settings['timeout_message'];
            $this->timeout_max_time = $this->settings['timeout_max_time'];
            $this->close_message_title = $this->settings['close_message_title'];
            $this->close_message = $this->settings['close_message'];
            $this->close_message_timer = $this->settings['close_message_timer'];

            $this->msg['message'] = '';
            $this->msg['class'] = '';

            add_action('init', array(&$this, 'check_culqi_response'));
            if (version_compare(WOOCOMMERCE_VERSION, '2.0.0', '>=')) {
                add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(&$this, 'process_admin_options'));
            } else {
                add_action('woocommerce_update_options_payment_gateways', array(&$this, 'process_admin_options'));
            }

            add_action('woocommerce_receipt_' . $this->id, array(&$this, 'receipt_page'));
        }

        public function is_available() {
            if ( $this->enabled === 'no' ) {
                return false;
            }

            // Culqi won't work without keys
            if ( ! $this->commerce_code && ! $this->secret_key && ! $this->api_endpoint) {
                return false;
            }

            return true;
        }

        function init_form_fields() {
            $this -> form_fields = array(
                'enabled' => array(
                    'title' => __('Habilitar/Deshabilitar', 'culqi'),
                    'type' => 'checkbox',
                    'label' => __('Habilitar pago con pasarela de Culqi.', 'culqi'),
                    'default' => 'no'),
                'title' => array(
                    'title' => __('Título', 'culqi'),
                    'type'=> 'text',
                    'description' => __('Controla el título que el usuario ve en el checkout..', 'culqi'),
                    'default' => __('Culqi', 'culqi')),
                'description' => array(
                    'title' => __('Descripción', 'culqi'),
                    'type' => 'textarea',
                    'description' => __('Controla la descripción que el usuario ve durante el checkout.', 'culqi'),
                    'default' => __('Paga de forma segura con tarjeta de crédito o débito.', 'culqi')),
                'testing' => array(
                    'title' => __('Modo de pruebas', 'culqi'),
                    'type' => 'checkbox',
                    'label' => __('Determina si se está usando el modo de pruebas o producción.'),
                    'default' => 'yes'
                ),
                'test_commerce_code' => array(
                    'title' => __('Código de comercio de prueba', 'culqi'),
                    'type' => 'text',
                    'description' => __('Este es el código de comercio de prueba.')),
                'test_secret_key' => array(
                    'title' => __('Llave secreta de prueba', 'culqi'),
                    'type' => 'text',
                    'description' =>  __('Esta es la llave secreta de prueba.', 'culqi'),
                ),
                'test_api_endpoint' => array(
                    'type'          => 'text',
                    'title'         => __( 'URL de Servidor de Integración de Culqi', 'culqi' )
                ),
                'live_commerce_code' => array(
                    'title' => __('Código de comercio de producción', 'culqi'),
                    'type' => 'text',
                    'description' => __('Este es código de comercio dado por Culqi.')),
                'live_secret_key' => array(
                    'title' => __('Llave secreta de producción', 'culqi'),
                    'type' => 'text',
                    'description' =>  __('Esta es la llave secreta de producción dado por Culqi.', 'culqi'),
                ),
                'live_api_endpoint' => array(
                    'type'          => 'text',
                    'title'         => __( 'URL de Servidor de Producción de Culqi', 'culqi' ),
                ),
                'redirect_page_id' => array(
                    'title' => __('Página de retorno'),
                    'type' => 'select',
                    'options' => $this -> get_pages('Select Page'),
                    'description' => __('Página luego de realizar el pago.')
                ),
                'timeout_message_title' => array(
                    'title' => __('Título de mensaje tiempo agotado'),
                    'type' => 'text',
                    'description' => __('Este es el título que aparece sobre el mensaje de tiempo agotado'),
                    'default' => __('Tiempo Agotado')
                ),
                'timeout_message' => array(
                    'title' => __('Mensaje de tiempo agotado'),
                    'type' => 'textarea',
                    'description' => __('Este es el mensaje que informa al cliente del tiempo agotado para realizar el pago.'),
                    'default' => __('Ha superado el tiempo límite para realizar el pago.')
                ),
                'timeout_max_time' => array(
                    'title' => __('Tiempo máximo para realizar el pago'),
                    'type' => 'text',
                    'description' => __('Este es el tiempo máximo (en segundos) para realizar el pago. Ej: 8 minutos => 480 segundos'),
                    'default' => '480'
                ),
                'close_message_title' => array(
                    'title' => __('Título de cierre del modal de Culqi'),
                    'type' => 'text',
                    'description' => __('Este es el título que aparece sobre el mensaje luego de cerrar el modal de Culqi'),
                    'default' => __('Cierre de Ventana de Pago')
                ),
                'close_message' => array(
                    'title' => __('Mensaje de cierre de modal de Culqi'),
                    'type' => 'textarea',
                    'description' => __('Este es el mensaje que informa al cliente del cierre del modal de pago de Culqi.'),
                    'default' => __('Usted ha cerrado el formulario de pago, intente nuevamente.')
                ),
                'close_message_timer' => array(
                    'title' => __('Tiempo para mostrar el mensaje de cierre del modal de Culqi'),
                    'type' => 'text',
                    'description' => __('Este es el tiempo que estará visible el mensaje de cierre del modal de Culqi en milisegundos. Ej: 5 segundos => 5000 milisegundos'),
                    'default' => '5000'
                )
            );
        }

        public function admin_options() {
            echo '<h3>' . __('Culqi Payment Gateway', 'culqi').'</h3>';
            echo '<p>' . __('Culqi Payment Input Requirements are: Order Number, Currency, Order Amount, Product Description, Customer Email, Billing Country, Billing City, Billing Customer Address, Billing Customer Phone, Billing Customer ID, Billing Customer Name and Billing Customer Lastname. For more information, you can see <a href="https://github.com/culqi/culqi-php" target="_blank">Culqi Documentation</a>.', 'culqi') . '</p>';
            echo '<table class="form-table">';
            // Generate the HTML for settings form
            $this->generate_settings_html();
            echo '</table>';
}

        /**
         * There are no payment fields for Culqi, but we want to show the description if set.
         */
        function payment_fields() {
            if($this->description) echo wpautop(wptexturize($this->description));
        }

        /**
         * Receipt Page
         */
        function receipt_page($order_id) {
            global $woocommerce;
            echo '<p>' . __('Gracias por su pedido, por favor de clic en el botón debajo de pagar con Culqi.', 'culqi') . '</p>';
            echo '<p>' . __('Nota: Usted puede intentar hasta <span id="culqi-attempts">3</span> veces en caso de su transacción sea rechazada.', 'culqi') . '</p>';
            $order = new WC_Order($order_id);
            echo $this->generate_culqi_form($order);
            wc_get_template( 'order/order-details.php', array(
                'order_id' => $order_id
            ) );
            echo $this->generate_form_html($order);
        }

        /**
         * Generate Culqi Button Link
         */
        public  function generate_culqi_form($order) {
            if (!isset($this->endpoint) || !isset($this->commerce_code) || !isset($this->secret_key)) {
                echo _('Se debe establecer la configuración para poder usar pagos mediante Culqi.');
                return;
            }

            $c4wc_api = new C4WC_API($this->endpoint, $this->commerce_code, $this->secret_key);
            $amount = intval($order->get_total() * 100);
            $currency = get_woocommerce_currency();

            $orderInfo = $c4wc_api->create_data_for_payment(array(
                "order_id" => (($this->testing == 'yes') ? "test" . $order->id : $order->id),
                "billing_country" => $order->billing_country,
                "billing_city" => $order->billing_city,
                "billing_address_1" => $order->billing_address_1,
                "billing_address_2" => $order->billing_address_2,
                "billing_phone" => $order->billing_phone,
                "billing_email" => $order->billing_email,
                "billing_first_name" => $order->billing_first_name,
                "billing_last_name" => $order->billing_last_name,
                "customer_id" => $order->customer_user,
                "order_description" => $this->get_cart_description($order->get_items()),
            ), $amount, $currency);

            $this->sales_info = $orderInfo;
            error_log('Ticket generado : ' . $this->sales_info . "\n", 3,  plugin_dir_path( __FILE__ ) . '/info.log');

            $protocol = is_ssl() ? 'https://' : 'http://';

            $redirect_url = ($this -> redirect_page_id=="" || $this -> redirect_page_id==0) ? $this->get_return_url( $order ) : get_permalink($this -> redirect_page_id);

            $c4wc_info = array(
                "commerceCode" => $this->commerce_code,
                "salesInfo" => $this->sales_info,
                "ajaxUrl" => admin_url( 'admin-ajax.php', $protocol ),
                "ajaxAction" => "check_culqi_payment",
                "redirectUrl" => $redirect_url,
                "redirectFailed" => $order->get_cancel_order_url(),
                "orderId" => $order->id,
                "timeout_message_title" => $this->timeout_message_title,
                "timeout_message" => $this->timeout_message,
                "timeout_max_time" => $this->timeout_max_time,
                "close_message_title" => $this->close_message_title,
                "close_message" => $this->close_message,
                "close_message_timer" => $this->close_message_timer
            );
            
            // Main culqi js
            wp_enqueue_script( 'culqi', '//integ-pago.culqi.com/api/v1/culqi.js', array(), '1.0', true );

            wp_localize_script( 'c4wc_js', 'c4wc_info', $c4wc_info);

            WC()->session->set( 'culqi_ticket' , $this->sales_info );
            WC()->session->set( 'culqi_order_id' , $order->id );

            return $this->generate_form_html($order);
        }

        public function generate_form_html($order) {
            return '<form action="" method="post" class="culqi_payment_form">
                        <input type="submit" class="button-alt submit_culqi_payment_form" value="'.__('Pagar con tarjeta de crédito / débito', 'culqi').'" />
                        <a class="button cancel" href="'.$order->get_cancel_order_url().'">'.__('Cancelar orden', 'culqi').'</a>
                    </form>';
        }

        /**
         * Process the payment and return the result
         */
        function process_payment($order_id) {
            $order = new WC_Order($order_id);
            return array(
                'result' => 'success',
                'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(get_option('woocommerce_pay_page_id'))))
            );
        }

        /**
         * Process refund
         *
         * Overriding refund method
         *
         * @access      public
         * @param       int $order_id
         * @param       float $amount
         * @param       string $reason
         * @return      mixed True or False based on success, or WP_Error
         */
        public function process_refund( $order_id, $amount = null, $reason = '' ) {

            $order = new WC_Order( $order_id );
            $transaction_id = $order->get_transaction_id();

            if (!$transaction_id) {
                return new WP_Error( 'culqi_refund_error',
                    sprintf(
                        __( '%s Reembolso falló porque el ticket de la transacción está faltando.', 'culqi' ),
                        get_class( $this )
                    )
                );
            }

            try {
                $c4wc_api = new C4WC_API($this->endpoint, $this->commerce_code, $this->secret_key);
                // Send the refund to Culqi API
                $refund_data = $c4wc_api->refund($transaction_id);
                error_log('Devolucion respuesta: ' . $refund_data["tipo_respuesta"] . ", cod: " . $refund_data["codigo_respuesta"] . ", mensaje: " . $refund_data["mensaje_respuesta"] . "\n", 3,  plugin_dir_path( __FILE__ ) . '/info.log');
                if (is_array($refund_data)) {
                    switch ($refund_data["tipo_respuesta"]) {
                        case "devolucion_exitosa":
                            $order->add_order_note(__('Pago con Culqi ha sido reembolasado. ', 'culqi'));
                            return true;
                            break;
                        case "comercio_invalido":
                            return new WP_Error( 'culqi_refund_error',
                                sprintf(
                                    __( '%s El comercio no está habilitado para hacer un reembolso.', 'culqi' ),
                                    get_class( $this )
                                )
                            );
                            break;
                        case "error_procesamiento":
                            return new WP_Error( 'culqi_refund_error',
                                sprintf(
                                    __( '%s Ha ocurrido un error mientras Culqi estaba procesando el reembolso.', 'culqi' ),
                                    get_class( $this )
                                )
                            );
                            break;
                    }
                }
            } catch (Exception $e) {
                error_log('Error en devolucion: ' . $e->getMessage() . "\n", 3,  plugin_dir_path( __FILE__ ) . '/info.log');
                return new WP_Error( 'culqi_refund_error',
                    sprintf(
                        __( '%s Falló el reembolso.', 'culqi' ),
                        get_class( $this )
                    )
                );
            }

            return false;
        }

        // get all pages
        function get_pages($title = false, $indent = true) {
            $wp_pages = get_pages('sort_column=menu_order');
            $page_list = array();
            if ($title) $page_list[] = $title;
            foreach ($wp_pages as $page) {
                $prefix = '';
                // show indented child pages?
                if ($indent) {
                    $has_parent = $page->post_parent;
                    while($has_parent) {
                        $prefix .=  ' - ';
                        $next_page = get_page($has_parent);
                        $has_parent = $next_page->post_parent;
                    }
                }
                // add to page list array array
                $page_list[$page->ID] = $prefix . $page->post_title;
            }
            return $page_list;
        }

        function get_cart_description($products) {
            $cart_description = "";
            foreach ($products as $product){
                $cart_description .= $product["name"] . " ";
            }
            $cart_description = trim($cart_description);
            if (strlen($cart_description) > 120) $cart_description = substr($cart_description, 0, 120);
            return $cart_description;
        }

    }
}

add_filter('woocommerce_payment_gateways', 'add_culqi_gateway');

/**
 * Add Cuqli Gateway to WooCommerce
 */
function add_culqi_gateway($methods)
{
    $methods[] = "WC_Culqi";
    return $methods;
}

// Scripts
add_action( 'wp_enqueue_scripts', 'load_scripts' );

function load_scripts() {
    if ( isset( $_GET['key'] ) || isset( $_GET['order'] ) ) {
        // Plugin js
        wp_enqueue_script( 'c4wc_js', plugins_url( 'assets/js/c4wc.js', __FILE__ ), array( 'culqi' ), '1.0', true);

        // Sweet Alert Javascript
        wp_enqueue_script( 'sa_js', plugins_url( 'assets/js/sweetalert.min.js', __FILE__ ), array(), '1.1', true);

        // Sweet Alert Style
        wp_enqueue_style( 'sa_css', plugins_url( 'assets/css/sweetalert.css', __FILE__ ));
    }
}

add_action( 'wp_ajax_check_culqi_payment', 'check_culqi_payment' );
add_action( 'wp_ajax_nopriv_check_culqi_payment', 'check_culqi_payment' );

 function check_culqi_payment() {
     global $woocommerce;
     $culqi_response = $_POST["response"];
     $order_id = $_POST["order_id"];
     $culqi = new WC_Culqi();
     $c4wc_api = new C4WC_API($culqi->endpoint, $culqi->commerce_code, $culqi->secret_key);
     $response = $c4wc_api->decipher_response($culqi_response);
     $response_validation = json_decode($response, true);
     error_log('Proceso de pago: ' . $response . "\n", 3,  plugin_dir_path( __FILE__ ) . '/info.log');
     if ($response_validation['codigo_respuesta'] == "venta_exitosa") {
         $order = new WC_Order($order_id);
         $order->payment_complete($response_validation['ticket']);
         $order->add_order_note(__('Pago con Culqi exitoso<br/>Ticket de Culqi: ', 'culqi') . $response_validation['ticket']);
         $order->add_order_note('Order paid');
         $woocommerce->cart->empty_cart();
         WC()->session->set("culqi_ticket", null);
         WC()->session->set("culqi_order_id", null);
     }
     echo json_encode($response);
     exit;
}

add_action( 'woocommerce_check_cart_items', 'check_if_second_attempt' );

function check_if_second_attempt() {
    $culqi_ticket = WC()->session->get("culqi_ticket") . "<br>";
    $culqi_order_id = WC()->session->get("culqi_order_id");
    if( is_cart() || is_checkout() ) {
        if (isset($culqi_ticket) && $culqi_ticket != null) {
            $order = new WC_Order($culqi_order_id);
            $order->cancel_order();
            WC()->session->set("culqi_ticket", null);
            WC()->session->set("culqi_order_id", null);
        }
    }

}