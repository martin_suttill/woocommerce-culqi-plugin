var Culqi = function() {
	this.commerceCode = c4wc_info.commerceCode;
	this.salesInfo = c4wc_info.salesInfo;
	this.isCheckingTime = false;
	this.timeout = null;
};

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

Culqi.prototype.getCommerceCode = function() {
	return this.commerceCode;
}

Culqi.prototype.getSaleInfo = function() {
	return this.salesInfo;
}

Culqi.prototype.getCheckingTime = function() {
	return this.isCheckingTime;
}

Culqi.prototype.setCheckingTime = function(value) {
	this.isCheckingTime = value;
}

Culqi.prototype.getTimeout = function() {
	return this.timeout;
}

Culqi.prototype.setTimeout = function(value) {
	this.timeout = value;
}

Culqi.prototype.payment_timeout = function() {
	if (gateway.getCheckingTime()) {
		var iframe = jQuery('#culqi_checkout_frame');
		if (iframe.length > 0) checkout.cerrar();
		swal({
			title: c4wc_info.timeout_message_title,
			text: c4wc_info.timeout_message,
			type: "warning",
			showConfirmButton: true,
			confirmButtonText: "Aceptar"
		}, function() {
			gateway.setCheckingTime(false);
			window.location = c4wc_info.redirectFailed;
		});
	}
}

Culqi.prototype.initTimeout = function() {
	gateway.setTimeout(setTimeout(gateway.payment_timeout, c4wc_info.timeout_max_time * 1000));
	gateway.setCheckingTime(true);
}

Culqi.prototype.cancelTimeout = function() {
	clearTimeout(gateway.getTimeout())
	gateway.setCheckingTime(false);
}

var gateway = new Culqi();

checkout.codigo_comercio = gateway.getCommerceCode();
checkout.informacion_venta = gateway.getSaleInfo();

function culqi(checkout) {
	var postData = {
		'action': c4wc_info.ajaxAction,
		'response': checkout.respuesta,
		'order_id': c4wc_info.orderId
	};
	if (checkout.respuesta == "checkout_cerrado") {
		jQuery(".submit_culqi_payment_form").removeAttr("disabled");
		gateway.cancelTimeout();
		swal({
			title: c4wc_info.close_message_title,
			text: c4wc_info.close_message,
			timer: c4wc_info.close_message_timer,
			showConfirmButton: false
		});
	} else {
		jQuery.ajax({
			type: "POST",
			dataType: "json",
			data: postData,
			url: c4wc_info.ajaxUrl,
			success: function (response) {
				var culqiResponse = JSON.parse(response);
				if (culqiResponse.codigo_respuesta == "venta_exitosa") {
					// Do logic for success
					checkout.cerrar();
					gateway.cancelTimeout();
					window.location = c4wc_info.redirectUrl;
				} else {
					checkout.cerrar();
					if (culqiResponse.codigo_respuesta == "venta_expirada") 
					{
						swal({
							title: c4wc_info.timeout_message_title,
							text: c4wc_info.timeout_message,
							type: "warning",
							showConfirmButton: true,
							confirmButtonText: "Aceptar"
						}, function() {
							gateway.setCheckingTime(false);
							window.location = c4wc_info.redirectFailed;
						});
					} else {
						gateway.cancelTimeout();
						var attempts = jQuery("#culqi-attempts").text();
						attempts--;
						if (attempts == 0) window.location = c4wc_info.redirectFailed;
						jQuery("#culqi-attempts").text(attempts);
						swal({
							title: "Mensaje",
							text: culqiResponse.mensaje_respuesta_usuario,
							type: "warning",
							showConfirmButton: true,
							confirmButtonText: "Aceptar"
						});
					}
				}
				jQuery(".submit_culqi_payment_form").removeAttr("disabled");
			},
			error: function (error) {
				alert("Ha ocurrido un error");
				checkout.cerrar();
				gateway.cancelTimeout();
				jQuery(".submit_culqi_payment_form").removeAttr("disabled");
			}
		});
	}
}

jQuery(document).ready(function() {
	jQuery(".submit_culqi_payment_form").on("click", function(e) {
		e.preventDefault();
		if (!gateway.getCheckingTime()) {
			checkout.abrir();
			gateway.initTimeout();
			jQuery(this).attr("disabled", "true");
		}
	});
	if (!isMobile.any()) {
		var btn = jQuery(".submit_culqi_payment_form")[0];
		btn.click();
	}
});